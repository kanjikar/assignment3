package Assignment3.ass3;

import static org.junit.Assert.*;

import org.junit.Test;

public class PerformanceTestCase {

	@Test
	public void testGetFriendsPerformance() {
	  String profileLink = "https://www.facebook.com/karim.kanji.1/friends";
	  long startTime = System.currentTimeMillis();
	  App.getFriends(profileLink);
	  long endTime = System.currentTimeMillis();

	  long duration = endTime - startTime;
	  assertTrue(duration < 5000); 
	}

}
