package Assignment3.ass3;

import static org.junit.Assert.*;

import org.junit.Test;

public class PositiveTestCase {

	@Test
	public void testGetFriends() {
	  String[] expectedFriends = {"Alice", "Bob", "Charlie"};
	  String[] actualFriends = App.getFriends("https://www.facebook.com/karim.kanji.1/friends");

	  assertArrayEquals(expectedFriends, actualFriends);
	}

}
