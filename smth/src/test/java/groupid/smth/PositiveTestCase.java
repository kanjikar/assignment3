package groupid.smth;
import org.junit.Test;

import groupid.smth.App.FacebookFriendList;
import static org.junit.Assert.*;

public class PositiveTestCase
{
	public class FacebookFriendList {
	    public String[] getFriendNames(String profileLink) {
	        return null; 
	    }
	}

    public class FacebookFriendListTest {
        @Test
        public void testGetFriendNames() {
            FacebookFriendList facebookFriendList = new FacebookFriendList();

            // Positive tests
            String[] actualNames1 = facebookFriendList.getFriendNames("https://www.facebook.com/johndoe");
            String[] expectedNames1 = {"Alice", "Bob", "Charlie"};
            assertArrayEquals(expectedNames1, actualNames1);

            String[] actualNames2 = facebookFriendList.getFriendNames("https://www.facebook.com/janesmith");
            String[] expectedNames2 = {"David", "Eve", "Frank", "Grace"};
            assertArrayEquals(expectedNames2, actualNames2);

            // Add more positive tests as needed
        }
    }
}
