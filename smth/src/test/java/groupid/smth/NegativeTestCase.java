package groupid.smth;
import org.junit.Test;

import groupid.smth.App.FacebookFriendList;
import static org.junit.Assert.*;

public class NegativeTestCase
{
	public class FacebookFriendList {
	    public String[] getFriendNames(String profileLink) {
	        return null; 
	    }
	}

	public class FacebookFriendListTest {
	    @Test
	    public void testGetFriendNames() {
	        FacebookFriendList facebookFriendList = new FacebookFriendList();

	        // Negative tests
	        String[] actualNames3 = facebookFriendList.getFriendNames("https://www.facebook.com/notauser");
	        assertNull(actualNames3);

	        String[] actualNames4 = facebookFriendList.getFriendNames(null);
	        assertNull(actualNames4);

	        // Add more negative tests as needed
	    }
	}
}
